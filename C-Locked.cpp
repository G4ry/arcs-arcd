#include <stdio.h>
#include <stdarg.h>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <iostream>
#include <sys/time.h>

using namespace cv;

using namespace std;

typedef char                                    IRTC8;
typedef unsigned char                           IRTU8;
typedef signed char                             IRTS8;
typedef unsigned short                          IRTU16;
typedef signed short                            IRTS16;
typedef unsigned int                            IRTU32;
typedef signed int                              IRTS32;
typedef float                                   IRTF32;

#ifndef IRT_DEBUG
#define IRT_DEBUG
#endif

#ifdef IRT_DEBUG
void IRTODS(const IRTC8 *DebugString,...);
#define IRTAssert(exp)  if (!(exp)) {IRTODS( "%s:%d\n",__FILE__, __LINE__ );while(1);}
#else
inline void IRTODS(const IRTC8 *DebugString,...){};
#define IRTAssert(exp)
#endif
#ifdef IRT_DEBUG
void IRTODS(const char *DebugString,...)
{
	va_list arg;
	va_start(arg, DebugString);
	static char str[1024];
	vsnprintf(str, 1024, DebugString, arg);
	va_end(arg);
    
	//slog2f(NULL, 9000, SLOG2_INFO, "%s", str);
    
	fprintf(stderr,"%s",str);
}
#endif

IRTU32 IRTMSec(void)
{
	timeval tNow;
	gettimeofday(&tNow, NULL);
	return ((IRTU32)(tNow.tv_sec*1000+tNow.tv_usec/1000));
}

//RIDGE G. Symons 11th November 2014
cv::Mat RIDGE(cv::Mat &BW,int Threshold)
{
    cv::Mat Result;
	Result.create(BW.rows,BW.cols,CV_8U);
    uint64_t t=(Threshold>>1),msk=0x7f7f7f7f7f7f7f7f,hs=0x8080808080808080;
    t|=t<<8;t|=t<<16;t|=t<<32;
    const int Offsets=16;
    const int Offset[Offsets]={
        -1-3*Result.step,-3*Result.step,-3*Result.step+1,
        -2-2*Result.step,2-2*Result.step,
        -3-Result.step,3-Result.step,
        -3,
        1+3*Result.step,3*Result.step,3*Result.step-1,
        2+2*Result.step,-2+2*Result.step,
        3+Result.step,-3+Result.step,
        3
    };
    
    
    for(int i=3+3*BW.step;i<((BW.step*(BW.rows-3))-3);i+=8)
    {
        uint64_t r=0,c=*((uint64_t *)(&BW.data[i]));
        c>>=1;c&=msk;c+=t;
        int j=0;
        for(int j=0;(j<(Offsets>>1));j++)
        {
            uint64_t o=*((uint64_t *)(&BW.data[i+Offset[j]]));
            o>>=1;o&=msk;o|=hs;o-=c;
            uint64_t n=*((uint64_t *)(&BW.data[i+Offset[j+(Offsets>>1)]]));
            n>>=1;n&=msk;n|=hs;n-=c;
            o&=n;
            r|=o;
        }
        r&=hs;
        *((uint64_t *)(&Result.data[i]))=r;
    }
    return Result;
}

float Length(cv::Point2f &a,cv::Point2f &b)
{
    cv::Point2f r=a-b;
    return sqrtf(r.x*r.x+r.y*r.y);
}

struct ARCS
{
    ARCS(cv::Point2f &c,float a,float r,cv::RotatedRect &rr){PosRadius=PosAngle=0;Center=c;Angle=a;Radius=r;RotRec=rr;}
    cv::Point2f Center;
    float Angle,Radius,PosAngle;
    cv::RotatedRect RotRec;
    int PosRadius;
};

static bool operator <(const ARCS &a,const ARCS &b)
{
    float aa=a.PosAngle+a.PosRadius*1e10;
    float ba=b.PosAngle+b.PosRadius*1e10;
    return aa<ba;
}

int CheckSum(int v)
{
    uint64_t s=0;
    for(int j=0;j<8;j++)
    {
        s^=1;
        s=(s<<1)|(s>>3);
        s+=(v>>(j<<2));
        s&=0xf;
    }
    return s;
}

string cl10name="C-Lock10-0";

cv::Mat CL10M(256,256,CV_8U);

void CLock10(int v)
{
    float lr=16,br=96;
    int lt=4;
    CL10M.setTo(0xff);
    cv::Point2f ci(128,128);
    cv::ellipse(CL10M,ci,cv::Size(120,120),0,0,360,cv::Scalar(0,0,0),lt);
    cv::ellipse(CL10M,ci,cv::Size(br,br),0,0,180,cv::Scalar(0,0,0),lt);
    int t=v;
    for(int i=7;i>=0;i--)
    {
        cv::Point2f c;
        float a=-(((i-1)&7)*acosf(-1)*2)/8;
        c=ci+cv::Point2f(64*cosf(a),64*sinf(a));
        int d=((t&0xf)*360)/16;
        t>>=4;
        cv::ellipse(CL10M,c,cv::Size(lr,lr),d,0,180,cv::Scalar(0,0,0),lt);
    }
    int s=CheckSum(v);
    int d=(s*360)/16;
    cv::ellipse(CL10M,ci,cv::Size(lr,lr),d,0,180,cv::Scalar(0,0,0),lt);
    imshow(cl10name,CL10M);
}

int AngleDiscrete(float Angle,int Bits,int Bias=(1<<(8-1)),int BiasRangeShift=8)
{
    int a=(Angle*(1<<(Bits+BiasRangeShift)))/(acosf(-1)*2);
    a=(a+Bias)>>BiasRangeShift;
    a&=((1<<Bits)-1);
    return a;
}

int main(int argc, char** argv)
{
    VideoCapture cap;
    cap.open(0);//"http://10.0.10.174:8080/videofeed?d=m.mjpg");
    if(!cap.isOpened()) return -1;
    
    namedWindow( "B&W", CV_WINDOW_AUTOSIZE );
    namedWindow( "RGBA", CV_WINDOW_AUTOSIZE );
    namedWindow( cl10name, CV_WINDOW_AUTOSIZE );
    namedWindow( "CL10 Found", CV_WINDOW_AUTOSIZE );
    
    IRTU32 FrameTime=0,Frames=0,CapturedFrames=0;
    int tv=16,ctv=5;
    int lastcl10=0,cl10=0,Invert=0;
    cv::createTrackbar("Threshold","B&W",&tv,255);
    cv::createTrackbar("Contour Threshold","B&W",&ctv,511);
    cv::Mat lf;
    int Filter=0;
    IRTODS("C-Locked-10 G.Symons 2014\n");
    IRTODS("When focused on windows use following keys in lower case:\n");
    IRTODS("q-:Quit  f:Filter i:Invert\ns:Save C10 Image  0-9/a-f:Cycle update 32 Bit ID Of C10 image\n");
    IRTODS("Test saved C10 by displaying it from a print out or transfer to a display on a phone for instance and put in front of this application's camera.\n");
    while(1)
    {
        cv::Mat RGBA,f,tf;
        const double *Rot,*Trans;
        cap >> RGBA;
        resize(RGBA,RGBA,Size(0,0),0.5f,0.5f,INTER_AREA);
        cvtColor(RGBA,f, CV_BGR2GRAY);
        if(Invert) cv::bitwise_not(f,f);
        
        IRTU32 mt=IRTMSec();
        
        tf=RIDGE(f,tv);
        if(lf.empty()||(!Filter)) f=tf; else cv::bitwise_and(tf,lf,f);
        lf=tf;

        mt=IRTMSec()-mt;
        //IRTODS( "FIRED at %d in %d ms.\n",tv,mt);
        imshow("B&W",f);
        
        mt=IRTMSec();
        std::vector<std::vector<cv::Point> > contours;
        std::vector<cv::Vec4i> hierarchy;
        findContours(f,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_NONE);
        mt=IRTMSec()-mt;
        //IRTODS( "Contours extracted %d contours in %d ms.\n", (int)contours.size(),mt);
        
        int c=waitKey(1);
        if(c=='q') break;
        if(c=='f') Filter^=1;
        if(c=='i') Invert^=1;
        if(c=='s') cv::imwrite(cl10name+".jpg",CL10M);
        if((c>='0')&&(c<='9')) cl10=(cl10<<4)|(c-'0');
        if((c>='a')&&(c<='f')) cl10=(cl10<<4)|(c-'a'+10);
        
        if(lastcl10!=cl10)
        {
            cv::destroyWindow(cl10name);
            std::stringstream stream;
            stream << std::hex << cl10;
            cl10name="CL10-0x"+stream.str();
            namedWindow( cl10name, CV_WINDOW_AUTOSIZE );
            lastcl10=cl10;
        }

        const int Bits=4;
        const float AngleTolerance=(acosf(-1)*2)/(1<<(Bits+1));
        const int sc=10;
        for(int i=0;i<(int)contours.size();i++)
        {
            if(contours[i].size()<5) continue;
            if(ctv<5) ctv=5;
            if(contours[i].size()<ctv) continue;
            cv::drawContours(RGBA,contours,i,cv::Scalar(i&1?255:0,i&2?255:0,i&4?255:0),1,8,hierarchy,0);
            if(hierarchy[i][2]<0) continue;
            int ci[sc];
            int rh=hierarchy[i][2],n=0;
            while(rh>=0)
            {
                if((hierarchy[rh][3]==i)&&(contours[rh].size()>=ctv))
                {
                    ci[n]=rh;
                    n++;
                }
                rh=hierarchy[rh][0];
            }
            if(n!=sc) continue;
            
            cv::RotatedRect cr[sc],cc=cv::fitEllipse(contours[i]);
            cv::ellipse(RGBA,cc,cv::Scalar(0,255,0));
            cv::Point2f rg[sc];
            std::vector<ARCS> ar;
            float minrc=1e5,maxr=-1e5;
            int minj,maxj;
            for(int j=0;j<sc;j++)
            {
                cr[j]=cv::fitEllipse(contours[ci[j]]);
                
                cv::Point2f o(0);
                for (int k=0;k<(int)contours[ci[j]].size();k++)
                {
                    cv::Point2f pt=contours[ci[j]][k];
                    o=o+pt;
                }
                o=o-(cr[j].center*int(contours[ci[j]].size()));
                rg[j]=o;

                float br=std::max(cr[j].size.width,cr[j].size.height)*0.5f;
                if(br>=maxr) {maxj=j;maxr=br;}
                float ba=atan2(rg[j].y,rg[j].x);
                ba=fmodf(ba,acosf(-1)*2);
                if(ba<0) ba+=acosf(-1)*2;
                ar.push_back(ARCS(cr[j].center,ba,br,cr[j]));
            }
            for(int j=0;j<sc;j++)
            {
                if(j!=maxj)
                {
                    float rc=Length(cr[j].center,cc.center);
                    if(rc<=minrc) {minj=j;minrc=rc;}
                }
                ar[j].PosAngle=AngleDiscrete(atan2(cr[j].center.y-cc.center.y,cr[j].center.x-cc.center.x)-ar[maxj].Angle,3);
            }
            ar[minj].PosRadius=1;
            ar[minj].PosAngle=0;
            ar[maxj].PosRadius=2;
            ar[maxj].PosAngle=0;
            
            std::sort(ar.begin(), ar.end());
            
            for(int j=0;j<sc;j++)
            {
                float r=ar[j].Radius,a=ar[j].Angle;
                cv::Point2f c=ar[j].Center;
                cv::Scalar Col;
                if(j<(sc-2))
                {
                    int l=0+((255*j)/(sc-3));
                    Col=cv::Scalar(l,l,l);
                }
                else
                    if(ar[j].PosRadius==1)
                        Col=cv::Scalar(0,0,255);
                    else
                        if(ar[j].PosRadius==2)
                            Col=cv::Scalar(255,0,0);
                        else
                            Col=cv::Scalar(0,255,255);
                cv::line(RGBA,c,cv::Point(c.x+cosf(a)*r,c.y+sinf(a)*r),Col);
                cv::ellipse(RGBA,ar[j].RotRec,Col);
            }

            uint64_t rt=0;
            for(int j=0;j<9;j++)
                {
                    uint64_t ra=AngleDiscrete(ar[j].Angle-ar[sc-1].Angle,Bits);
                    rt>>=Bits;rt|=ra<<32;
                }
            
            int s=CheckSum(rt);
            
            {
                IRTODS("\rFound:0x%08x:%01x(%s:%01x) Frame:%d                     ",(unsigned int)rt,(unsigned int)((rt>>32)&0xf),(((rt>>32)&0xf)!=s)?"Fail:":"Pass:",s,Frames);
                float l=std::max(cc.size.width,cc.size.height)*0.5f;
                cv::Rect r(cc.center.x-l,cc.center.y-l,2*l,2*l);
                if((r.x>0)&&(r.y>0)&&(r.x+r.width<=RGBA.cols)&&(r.y+r.height<=RGBA.rows))
                {
                    cv::Mat fcl10=RGBA(r),wd=Mat::zeros(256,256,fcl10.type()),wdr;
                    cv::Point2f fcl10c=cv::Point2f(fcl10.cols*0.5f,fcl10.rows*0.5f);
                    cv::Point2f ev[4];
                    cc.points(ev);
                    cv::Point2f ep[2]={ev[0]+(ev[2]-ev[1])*0.5f,ev[1]+(ev[3]-ev[2])*0.5f};
                    cv::Point2f t[2][3]={
                        {ep[0]-cv::Point2f(r.x,r.y),ep[1]-cv::Point2f(r.x,r.y),cc.center-cv::Point2f(r.x,r.y)},
                        {cv::Point2f(128,128+120),cv::Point2f(128+120,128),cv::Point2f(128,128)}
                    };
                    cv::Mat Warp=cv::getAffineTransform(t[0],t[1]);
                    cv::warpAffine(fcl10,wd,Warp,wd.size());

                    std::vector<cv::Point2f> vt,tv;
                    vt.push_back(cv::Point2f(ar[0].RotRec.center)-cv::Point2f(r.x,r.y));
                    cv::transform(vt,tv,Warp);
                    float ba=atan2(tv[0].y-128,tv[0].x-128)-acosf(-1)*0.5f;
                    ba=fmodf(ba,acosf(-1)*2);
                    if(ba<0) ba+=acosf(-1)*2;
                    cv::Mat RotMat=getRotationMatrix2D(cv::Point2f(128,128),ba*180/acosf(-1),1);
                    warpAffine(wd,wdr,RotMat,wd.size());
                    
                    imshow("CL10 Found",wdr);
                }
            }
        }
        
        imshow("RGBA",RGBA);
        CLock10(cl10);
        Frames++;
        int dt=(IRTMSec()-FrameTime);
        //if(dt>0) IRTODS("%dx%d FPS:%d %d Filter:%s\n",f.cols,f.rows,(Frames*1000)/dt,(CapturedFrames*1000)/dt,Filter?"On":"Off");
        if(!(Frames&15))
        {
            Frames=0;
            CapturedFrames=0;
            FrameTime=IRTMSec();
        }
    }
    return 0;
}

