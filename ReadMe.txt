ARCS & ARCD
Angularly (Affine) Ratifiable Contours Signature/Descriptor
Released under the open source MIT License (see end)
Copyright (c) 2014 G.Symons

Angularly (Affine) Ratifiable Contours Signature

All the power of QR codes but with artistic license.

An edge/threshold detector and resulting list of separated contours are found in a captured image.
A recognisable frame of reference is any separate contour larger in area than its closely neighbouring smaller contours.
Each smaller contour having order and differing orientations, based on their shape, relative to the larger reference.
Each orientation represents sequential groups of bits of a full code word.
The full code can then be, if required, self verified by a built in checksum.
Error correction is simply the trial of permutations of slight variations in orientations until the full code self verifies.

For example, a picture of a small box (rectangle), and adjacent to one corner, 8 much smaller than box, matchsticks (lines).
These are variously orientated in known positions relative to box.
These in all can represent 8x4 bits 32 bits, each orientation quantised to 16 levels 4 bits.
The first 24 bits can be the actual code and the last 8 the checksum, crc, or hash etc.

An enhanced version can use a picture of a moon silhouette with silhouette rockets in various rotations orbiting it.
Using both the rockets contour orientations and order relative to the asymmetrical moon's contour the code can be found.
The code can then be verified and, if need be, corrected.

C-Locked is a version where inside a large C there are smaller Cs of varying size. These could be inter-nested for instance, decreasing in size. The contained Cs are independently rotated, scaled and positioned relative to the large containing C to collectively represent a recoverable code. Each C is similar, an n sided polygon with one side missing, where n is the number of discrete rotations allowed. For instance a square with 3 filled sides and one open has 4 possible rotations, with 8 nested that is 16 bits, an octagon 8, with 8 nested 24 bits etc. A square C is usually used as it is the most robust. The polygons are easily recovered to find the represented code, even under noisy conditions, and can also be tracked with the resulting pose matrix easily found.

A very robust and artistically flexible version of ARCS can search each recognisable contour, such as a bold C shape, in a captured image and then simply test intensity gradients at various pre-designated positions within the bold enclosing contour. The regions at those positions having been pre-encoded by orientating line drawn or grey scale icons which have a pattern with a strong orientation. The homography of the enclosing contour can pin point the pre-designated positions accurately in cases of extreme close up warping. The code is assembled as usual from the relative orientations of the pin pointed regions in relation to the enclosing bold contour and can be verified as usual by the code's appended check sum etc.


Angularly (Affine) Ratifiable Contours Descriptor

A similar system can be used for homography based image recognition.
This is done by finding a set of large separated contours surrounded by smaller separated contours.
The set from any reference image can identify a similar warped image within captured footage even if some of the set is occluded.
In effect each element of that set is an Angularly Ratifiable Contours Descriptor, ARCD.

Affine Enhancements

The system can be extended with affine enhancements.
Such as ratios of neighbouring separated contour scales being incorporated into encoding.

Released under the MIT License.

The MIT License (MIT)

Copyright (c) 2014 G.Symons

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.